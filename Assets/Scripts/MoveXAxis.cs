﻿using UnityEngine;
using System.Collections;

public class MoveXAxis : MonoBehaviour {

	public int zAxis; //public int so I can set the z axis value in Unity, this is to set the location of the obstacle in the level

	private Vector3 pos1;
	private Vector3 pos2;
	public float speed = 1.0f;

	void Start(){
		pos1 = new Vector3(-3,2,zAxis);
		pos2 = new Vector3(3,2,zAxis);
	}

	void Update() {
		transform.position = Vector3.Lerp (pos1, pos2, (Mathf.Sin(speed * Time.time) + 1.0f) / 2.0f);
	}
}
