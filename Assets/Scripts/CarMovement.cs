﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CarMovement : MonoBehaviour {

    
    public float forwardSpeed;

    public float leftRightSpeed;

    private Rigidbody rb;

    private bool grounded;

	private int score;

	public GameObject gameOverScreen;
	public GameObject finishScreen;
	public GameObject UI;

	public AudioClip jumpSound;
	public AudioClip collectSound;
	public AudioClip deathSound;

	[SerializeField]
	private Text scoreText = null;
	[SerializeField]
	private Text finishScore = null;
	[SerializeField]
	private Text gameOverScore = null;


    // Use this for initialization
    void Start () {

		score = 0;

		Time.timeScale = 1;

        rb = GetComponent<Rigidbody>();

        grounded = true;

        Physics.gravity = new Vector3(0, -15.0F, 0);

    }
	
	// Update is called once per frame
	void Update () {



        transform.position += Vector3.forward * Time.deltaTime * forwardSpeed;

  

        if (grounded == false) //if player is in the air, rotate 180 degrees
        {
    

            Vector3 rotTemp = transform.rotation.eulerAngles;

            rotTemp.z += 140f * Time.deltaTime;

            transform.rotation = Quaternion.Euler(rotTemp);

        }

		if ((Input.GetButtonDown("Jump") || Input.GetButtonDown("AButton")) && grounded == true)
        {
			AudioSource audio = GetComponent<AudioSource> ();
			audio.clip = jumpSound;
			audio.Play ();


            rb.velocity = new Vector3(0, 17, 0);

            grounded = false;



        }


		if((Input.GetKey("a") || Input.GetAxis("DPADHorizontal")<0 || Input.GetKey(KeyCode.LeftArrow)) && grounded == true) //left right input, uses a key, dpad left on xbox controller or left arrow on keyboard
        {
            rb.velocity = new Vector3(-5, 0, 0);
        }
		if((Input.GetKey("d") || Input.GetAxis("DPADHorizontal")>0.001 || Input.GetKey(KeyCode.RightArrow)) && grounded == true)
        {
            rb.velocity = new Vector3(5, 0, 0);
        }
    }
    private void OnCollisionEnter (Collision col)
    {
        if(col.gameObject.tag == "FloorRed") //if colliding with red floor and player blue side is facing down, call gameOver, player colour side is checked using rotation
        {
            grounded = true;

            if (transform.up.y > 0.6)
            {

				gameOver ();
            }

        }
        if (col.gameObject.tag == "FloorBlue")
        {
            grounded = true;

            if (transform.up.y < -0.6)
            {

				gameOver ();
            }

        }
        if (col.gameObject.tag == "Obstacle")
        {
           
			gameOver ();
        }
        if (col.gameObject.tag == "FloorGreen")
        {
            grounded = true;

        }
		if (col.gameObject.tag == "Finish")
		{
			Time.timeScale = 0;

			finishScreen.SetActive (true);

			finishScore.text = "Score: " + score;

			UI.SetActive (false);

		}
		if (col.gameObject.tag == "ScoreBall") 
		{
			AudioSource audio = GetComponent<AudioSource> ();
			audio.clip = collectSound;
			audio.Play ();

			Destroy (col.gameObject);

			score = score + 100;

			scoreText.text = "" + score;
		}
    }
	void gameOver() //game over method: play game over sound, set game to freeze, show game over canvas, hide game UI
	{
		AudioSource audio = GetComponent<AudioSource> ();
		audio.clip = deathSound;
		audio.Play ();

		Time.timeScale = 0;

		gameOverScreen.SetActive (true);

		gameOverScore.text = "Score: " + score;

		UI.SetActive (false);

	}
}
