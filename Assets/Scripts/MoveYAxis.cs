﻿using UnityEngine;
using System.Collections;

public class MoveYAxis : MonoBehaviour {

	public int zAxis;

	private Vector3 pos1;
	private Vector3 pos2;
	public float speed = 1.0f;

	void Start(){
		pos1 = new Vector3(0,1,zAxis);
		pos2 = new Vector3(0,3,zAxis);
	}

	void Update() {
		transform.position = Vector3.Lerp (pos1, pos2, (Mathf.Sin(speed * Time.time) + 1.0f) / 2.0f);
	}
}

