﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour {

    public float forwardSpeed = 8f;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        transform.position += Vector3.forward * Time.deltaTime * forwardSpeed;
    }
}
