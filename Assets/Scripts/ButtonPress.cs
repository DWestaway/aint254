﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ButtonPress : MonoBehaviour {

	public string scene;

	public void loadScene()
	{
		StartCoroutine (newScene ());

	}
	IEnumerator newScene() //load new scene, scene name is inputted in Unity, uses a 0.6 second wait so sound plays before the scene is changed
	{
		yield return new WaitForSeconds (0.6f);

		SceneManager.LoadScene (scene);
	}
}
