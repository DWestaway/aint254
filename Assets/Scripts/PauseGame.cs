﻿using UnityEngine;
using System.Collections;

public class PauseGame : MonoBehaviour {

	public GameObject pauseScreen;
	private bool paused;

	// Use this for initialization
	void Start () {
		Time.timeScale = 1;
	}
	
	// Update is called once per frame
	void Update () {


	
		if (Input.GetKeyDown ("escape")) 
		{
			paused = !paused;

			pauseScreen.SetActive(paused);

			if (paused == true) {
				Time.timeScale = 0;
			}
			if (paused == false) {
				Time.timeScale = 1;
			}
		}

	}
}
